module Board = {
  type uninitializedBoard;
  type t;
  [@bs.module "johnny-five"] [@bs.new]
  external make : unit => uninitializedBoard = "Board";
  [@bs.send]
  external getInitializedBoard :
    (uninitializedBoard, [@bs.as "ready"] _, [@bs.this] (t => unit)) => unit =
    "on";
  let getInitializedBoard = uninitializedBoard =>
    Js.Promise.make((~resolve, ~reject as _reject) =>
      uninitializedBoard |. getInitializedBoard([@bs.this] t => resolve(. t))
    );
};

let foo = Board.make() |. Board.getInitializedBoard;
/* TODO:
   Accelerometer
   Altimeter
   Animation
   Barometer
   Boards
   BsJ5
   Button
   Compass
   Esc
   Escs
   Expander
   Fn
   Gps
   Gyro
   Hygrometer
   Imu
   IrReflectArray
   Joystick
   Keypad
   Lcd
   Led
   LedDigits
   LedMatrix
   LedRgb
   Leds
   Light
   Motion
   Motor
   Motors
   Multi
   Piezo
   Pin
   Proximity
   Relay
   Relays
   Sensor
   Servo
   Servos
   Shiftregister
   Stepper
   Switch
   Thermometer
   */
